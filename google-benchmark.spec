%global intname benchmark
%global lbname lib%{intname}

Name: google-benchmark
Version: 1.7.1
Release: 2

License: Apache-2.0
Summary: A microbenchmark support library
URL: https://github.com/google/%{intname}
Source0: %{url}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires: gtest-devel gmock-devel ninja-build gcc-c++ cmake doxygen gcc

%description
A library to support the benchmarking of functions, similar to unit-tests.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description devel
%{summary}.

%package_help

%prep
%autosetup -n %{intname}-%{version} -p1
sed -e '/get_git_version/d' -e '/-Werror/d' -i CMakeLists.txt

%build
%cmake -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DGIT_VERSION=%{version} \
    -DBENCHMARK_ENABLE_DOXYGEN:BOOL=ON \
    -DBENCHMARK_ENABLE_TESTING:BOOL=ON \
    -DBENCHMARK_USE_BUNDLED_GTEST:BOOL=OFF \
    -DBENCHMARK_ENABLE_GTEST_TESTS:BOOL=ON \
    -DBENCHMARK_ENABLE_INSTALL:BOOL=ON \
    -DBENCHMARK_INSTALL_DOCS:BOOL=ON \
    -DBENCHMARK_DOWNLOAD_DEPENDENCIES:BOOL=OFF
%cmake_build

%check
%ctest

%install
%cmake_install

%files
%doc CONTRIBUTING.md README.md
%license AUTHORS CONTRIBUTORS LICENSE
%{_libdir}/%{lbname}*.so.1*

%files devel
%{_libdir}/%{lbname}*.so
%{_includedir}/%{intname}/
%{_libdir}/cmake/%{intname}/
%{_libdir}/pkgconfig/%{intname}.pc

%files help
%{_docdir}/%{intname}/

%changelog
* Tue Nov 12 2024 Funda Wang <fundawang@yeah.net> - 1.7.1-2
- adopt to new cmake macro

* Wed Sep 06 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 1.7.1-1
- Package init
